<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationUserPivot extends Model
{
    protected $fillable = [
        'user_id', 'conversations_id'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function conversations()
    {
        return $this->belongsTo(Conversations::class);
    }
}
