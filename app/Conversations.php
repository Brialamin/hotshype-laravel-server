<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversations extends Model
{
    protected $fillable = [
        'name'
    ];

    public function messages()
    {
        return $this->hasMany(Messages::class);
    }

    public function users()
    {
    	return $this->belongsToMany(User::class, 'conversation_user_pivots');
    }

    public function lastMessage(){
        return $this->hasOne(Messages::class)->latest();
    }	

}
