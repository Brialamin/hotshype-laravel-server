<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use App\Teams;
use App\User;
use App\UserData;
use App\Recruitment;
use App\Conversations;
use App\Messages;
use App\TeamUserPivot;
use App\ConversationUserPivot;
use App\Role;

class MessageController extends Controller
{
    public function loadMessages($conversation)
    {
        Log::info("Conversation id: ".$conversation);
        $conversation = Conversations::find($conversation);
        foreach($conversation->messages as $message)
        {
            $from = $message->from;
            $convo = $message->conversation;
        }
            

        return json_encode($conversation->messages);
    }

    public function createMessage(Request $request)
    {
        $input = $request->all();

        Log::info("Input: ".http_build_query($input));

        $conversation_id = 0;

        $new_conversation = true;

        $message = $input['message'];
        $from_id = $input['from_id'];
        $conversation_id = $input['conversations_id'];

        if($message == null)
            $message = "I forgot to put a message before hitting send kappa";

        $message_obj = new Messages();

        $message_obj->message = $message;
        $message_obj->from_id = $from_id;
        $message_obj->conversations_id = $conversation_id;

        if($message_obj->save())
            return json_encode("success");

        return json_encode("fail");
    }

    public function getUsers($conversation)
    {
        $conversation = Conversations::find($conversation);
        return $conversation->users;
    }

    public function getLastMessage($conversation)
    {
        $conversation = Conversations::find($conversation);
        $from = $conversation->lastMessage->from;
        return json_encode($conversation->lastMessage);

    }
}
