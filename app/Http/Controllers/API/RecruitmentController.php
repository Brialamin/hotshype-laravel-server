<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
use App\Teams;
use App\User;
use App\UserData;
use App\Recruitment;
use App\Conversations;
use App\Messages;
use App\TeamUserPivot;
use App\ConversationUserPivot;
use App\Role;

class RecruitmentController extends Controller
{
    public function updateRole(Request $request, $role)
    {
        $input = $request->all(); 

        $passed_role = Recruitment::find($role);

        $is_selected = $input['selected'];

        $passed_role->selected = $is_selected;

        if($passed_role->save())
            $response = "success";
        else
            $response = "failed";

        return response()->json($response, 200);    
    }
}
