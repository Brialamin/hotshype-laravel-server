<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Teams;
use App\User;
use App\UserData;
use App\Recruitment;
use App\Conversations;
use App\Messages;
use App\TeamUserPivot;
use App\ConversationUserPivot;
use App\Role;

class TeamController extends Controller
{
    
    public function index()
    {
        return Teams::with(['teamCaptain','lookingForRoles'])->get();
    }

    public function new(Request $request)
    {
        return Teams::create($request->all);
    }

    public function show($team)
    {
        $team_obj = Teams::with('teamCaptain')->find($team);
        return $team_obj;
    }

    public function lookingForPlayers()
    {
        return Teams::with(['teamCaptain','lookingForRoles'])->where('recruiting', 1)->get();
    }

    public function manageTeam($team_id)
    {
        $team = Teams::find($team_id);
        $recruit_status = Recruitment::where('team_id', $team_id)->get();

        return view('manageTeam')->withTeam($team)->withRoles($recruit_status);
    }

    public function updateTeam(Request $request, $team_id)
    {
        $input = $request->all();

        $name = $input['new_name'];

        $roles = Recruitment::where('team_id', $team_id)->orderBy('id', 'asc')->get();
        $team = Teams::find($team_id);

        if($request->has('lfm'))
            $recruit_status = 1;
        else
            $recruit_status = 0;

        foreach($roles as $role)
        {
            if($request->has($role->role_id))
            {
                $role->selected = 1;
            }
            else
            {
                $role->selected = 0;
            }
            $role->save();
        }

        $team->recruiting = $recruit_status;
        $team->name = $name;
        if($team->save())
        {
            return back()->withCreated(true);
        }
        return back()->withCreated(false);
    }
}
