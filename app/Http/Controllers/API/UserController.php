<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Teams;
use App\User;
use App\UserData;
use App\Recruitment;
use App\Conversations;
use App\Messages;
use App\TeamUserPivot;
use App\ConversationUserPivot;
use App\Role;

class UserController extends Controller
{
    
    public function index()
    {
        return User::with('roles')->get();
    }

    public function show($user)
    {
        if(is_numeric($user))
        {   
            return User::with(['userData', 'roles'])->find($user);
        }
        else
        {
            return User::with('userData')->where('email', urldecode($user))->first();
        }
    }

    public function getUserMessages($user)
    {
        $user = User::find($user);
        $conversations = $user->conversations;
        $last_messages = array();
        foreach($conversations as $conversation)
        {
            $last_message = $conversation->lastMessage;
            $from = $last_message->from;
            $convo = $last_message->conversation;
            array_push($last_messages, $last_message);

        }

        return json_encode($last_messages);
        
    }

    public function usersTeams($user_id)
    {
        $user = User::find($user_id);
        $teams = $user->teams;

        $response = array('teams' => $teams);

        return response()->json($response, 200);
    }

    public function getUserConversations($user)
    {
        $user_sent = User::find($user);
        return json_encode($user_sent->conversations);
    }

    public function new(Request $request)
    {
        return User::create($request->all);
    }

    public function updateUser(Request $request, $user)
    {
        $input = $request->all(); 

        Log::info("Showing request: ".http_build_query($input));

        $bnet = $input['user_data']['bnet_tag'];
        $twitch = $input['user_data']['twitch_name'];
        $recruit_status = $input['user_data']['recruitable'];

        Log::info("Recruitable: ".$recruit_status);

        UserData::updateBNetAccount($user, $bnet);
        UserData::updateTwitchAccount($user, $twitch);
        UserData::updateIsRecruitable($user, $recruit_status);

        return response()->json("success", 200);
    }

    public function getRecruitableUsers()
    {
        $players = UserData::where('recruitable', 1)->orderBy('id', 'asc')->get();
        $users = array();
        foreach($players as $player)
        {
            $roles = $player->user->roles;
            array_push($users, $player->user);

        }
        //$players->roles;

        return json_encode($users);
    }

    public function register(Request $request)
    {
        $this->validator($request->all());

        $user = $this->create($request->all());

        return json_encode($user);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {

        $bnet = $data['battlenet-tag'];
        $twitch = $data['twitch-name'];

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $user_data = UserData::create([
            'user_id' => $user->id,
            'twitch_name' => $twitch,
            'bnet_tag' => $bnet,
            'user_type' => 'Player',
            ]);

        $recruit_array = array();
        for($i = 1; $i < 10; $i++)
        {
            $recruitment = new Recruitment();
            $recruitment->user_id = $user->id;
            $recruitment->role_id = $i;
            $recruitment->save();
        }

        $user->userData;
        $user->roles;

        return $user;
    }

    protected function updateUserNotificationToken(Request $request, $user_id)
    {
        $input = $request->all();

        Log::info("Showing request: ".http_build_query($input));
        $user = User::find($user_id);
        $user_data = $user->userData;
        $user_data->notification_token = $input[0];

        if($user_data->save())
            return json_encode('success');

        return json_encode('fail');
    }

    /**
        Everything below this point
        Is garbage code that needs 
        to be gone through to see if
        there's anything useful for 
        the api
    */

    public function update(Request $request, $id)
    {
        $team = Teams::findOrFail($id);
        $team->update($request->all);
    }

    public function lookingForTeam()
    {
        $players = UserData::where('recruitable', 1)->orderBy('id', 'asc')->pluck('user_id')->toArray();
        $players_lft = Recruitment::whereIn('user_id', $players)->where('selected', 1)->get();

        return view('needTeam')->withRoles($players_lft);
    }

    public function lookingForPlayers()
    {
        $teams = Teams::where('recruiting', 1)->orderBy('id', 'asc')->pluck('id')->toArray();
        $teams_lfm = Recruitment::whereIn('team_id', $teams)->where('selected', 1)->get();

        return view('needPlayer')->withRoles($teams_lfm);
    }

    public function manageTeam($team_id)
    {
        $team = Teams::find($team_id);
        $recruit_status = Recruitment::where('team_id', $team_id)->get();

        return view('manageTeam')->withTeam($team)->withRoles($recruit_status);
    }

    public function updateTeam(Request $request, $team_id)
    {
        $input = $request->all();

        $name = $input['new_name'];

        $roles = Recruitment::where('team_id', $team_id)->orderBy('id', 'asc')->get();
        $team = Teams::find($team_id);

        if($request->has('lfm'))
            $recruit_status = 1;
        else
            $recruit_status = 0;

        foreach($roles as $role)
        {
            if($request->has($role->role_id))
            {
                $role->selected = 1;
            }
            else
            {
                $role->selected = 0;
            }
            $role->save();
        }

        $team->recruiting = $recruit_status;
        $team->name = $name;
        if($team->save())
        {
            return back()->withCreated(true);
        }
        return back()->withCreated(false);
    }

    public function messagePlayer($user_id)
    {
        $player = User::find($user_id);

        return view('messagePlayer')->withPlayer($player);
    }

    public function showTeam($team_id)
    {
        $team = Teams::find($team_id);

        return view('displayTeam')->withTeam($team);
    }

    public function leagues()
    {
        return view('comingSoon');
    }

    public function scrims()
    {
        return view('comingSoon');   
    }

    public function faq()
    {
        return view('comingSoon');
    }

    public function matches($type)
    {
        return view('comingSoon');
    }

    public function sendMessage(Request $request, $user_id)
    {
        $input = $request->all();

        $from_user = Auth::user();

        $conversation_id = 0;

        $new_conversation = true;

        foreach($from_user->conversations as $conversation)
        {
            $users = $conversation->users;
            foreach($users as $user)
            {
                if($user->id == Auth::id())
                {   
                    continue;
                }
                else if($user->id == $user_id)
                {
                    $conversation_id = $conversation->id;
                }

            }
            if($conversation_id !== 0)
            {
                $new_conversation = false;
                break;
            }
        }

        if($new_conversation)
        {
            $conversation = new Conversations();
            $conversation->save();
            $conversation_id = $conversation->id;

            $conversation_user_pivot = new ConversationUserPivot();
            $conversation_user_pivot->user_id = $from_user->id;
            $conversation_user_pivot->conversation_id = $conversation_id;
            $conversation_user_pivot->save();

            $conversation_user_pivot = new ConversationUserPivot();
            $conversation_user_pivot->user_id = $user_id;
            $conversation_user_pivot->conversation_id = $conversation_id;
            $conversation_user_pivot->save();
        }



        $message = $input['message'];

        if($message == null)
            $message = "I forgot to put a message before hitting send kappa";

        $message_obj = new Messages();

        $message_obj->message = $message;
        $message_obj->from_id = $from_user->id;
        $message_obj->conversations_id = $conversation_id;

        if($message_obj->save())
            return back()->withCreated(true);

        return back()->withCreated(false);
    }

    public function viewConversation($conversation_id)
    {
        $conversation = Conversations::find($conversation_id);

        $not_logged_in_user = 0;

        foreach($conversation->users as $user)
        {   
            if($user != Auth::user())
            {
                $not_logged_in_user = $user;
            }
        }

        return view('displayConversation')->withConversation($conversation)->withOther_user($not_logged_in_user);
    }
}
