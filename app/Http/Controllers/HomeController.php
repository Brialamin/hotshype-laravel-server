<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Auth;
use Storage;
use File;
use App\Teams;
use App\User;
use App\UserData;
use App\Recruitment;
use App\Conversations;
use App\Messages;
use App\TeamUserPivot;
use App\ConversationUserPivot;
use App\TeamJoinRequests;
use App\PlayerInvites;
use App\Push;
use App\Firebase;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
                'userProfile',
                'usersTeams',
                'index',
                'teams',
                'lookingForTeam',
                'lookingForPlayers',
                'showTeam',
                'leagues',
                'matches',
                'scrims',
                'faq',
            ]]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function createTeam(Request $request)
    {
        $input = $request->all();

        $name = $input['new_name'];
        $captain = Auth::id();

        $team = new Teams();
        $pivot = new TeamUserPivot();

        $team->name = $name;
        $team->captain = $captain;
        $team->logo_location = "placeholder_icon.png";

        if($team->save())
        {
            for($i = 1; $i < 10; $i++)
            {
                $recruitment = new Recruitment();
                $recruitment->team_id = $team->id;
                $recruitment->role_id = $i;
                $recruitment->save();
            }
            $pivot->teams_id = $team->id;
            $pivot->user_id = $captain;
            $pivot->is_captain = 1;
            if($pivot->save())
                return redirect()->action('HomeController@index')->withCreated(true);

        }
        return redirect()->action('HomeController@index')->withCreated(false);
    }

    public function new()
    {
        return view('new');
    }

    public function usersTeams($user_id)
    {
        $user = User::find($user_id);
        $teams = $user->teams;

        return view('teams')->withTeams($teams);
    }

    public function teams()
    {
        $teams = Teams::orderBy('id', 'asc')->get();
        return view('teams')->withTeams($teams);
    }

    public function userProfile($user_id)
    {
        $user = User::find($user_id);
        $user_data = $user->userData;
        $roles = Recruitment::where('user_id', $user_id)->orderBy('id', 'asc')->get();
        $conversations = $user->conversations;
        $invites = $user->invites;
        $requests = $user->teamRequests;

        return view('user')->withData($user_data)
                           ->withName($user->name)
                           ->withId($user_id)
                           ->withRoles($roles)
                           ->withConversations($conversations)
                           ->withInvites($invites)
                           ->withRequests($requests);
    }

    public function updateUser(Request $request, $user_id)
    {
        $input = $request->all();

        $roles = Recruitment::where('user_id', $user_id)->orderBy('id', 'asc')->get();

        $bnet = $input['bnet'];
        $twitch = $input['twitch'];
        if($request->has('lft'))
            $recruit_status = 1;
        else
            $recruit_status = 0;

        foreach($roles as $role)
        {
            if($request->has($role->role_id))
            {
                $role->selected = 1;
            }
            else
            {
                $role->selected = 0;
            }
            $role->save();
        }        

        UserData::updateBNetAccount($user_id, $bnet);
        UserData::updateTwitchAccount($user_id, $twitch);
        UserData::updateIsRecruitable($user_id, $recruit_status);

        return back()->with('success', 'Successfully updated your information!');
    }

    public function lookingForTeam()
    {
        $players = UserData::where('recruitable', 1)->orderBy('id', 'asc')->pluck('user_id')->toArray();
        $players_lft = Recruitment::whereIn('user_id', $players)->where('selected', 1)->get();

        return view('needTeam')->withRoles($players_lft);
    }

    public function lookingForPlayers()
    {
        $teams = Teams::where('recruiting', 1)->orderBy('id', 'asc')->pluck('id')->toArray();
        $teams_lfm = Recruitment::whereIn('team_id', $teams)->where('selected', 1)->get();

        return view('needPlayer')->withRoles($teams_lfm);
    }

    public function manageTeam($team_id)
    {
        $team = Teams::find($team_id);
        $recruit_status = Recruitment::where('team_id', $team_id)->get();
        $join_requests = $team->joinRequests;
        $invites_sent = $team->invites;

        return view('manageTeam')->withTeam($team)->withRoles($recruit_status)->withRequests($join_requests)->withInvites($invites_sent);
    }

    public function updateTeam(Request $request, $team_id)
    {
        $input = $request->all();

        $roles = Recruitment::where('team_id', $team_id)->orderBy('id', 'asc')->get();
        $team = Teams::find($team_id);

        $logo; $file_name;

        $name = $input['new_name'];

        //process image info if an image was uploaded
        if(array_key_exists('logo', $input))
        {
            $logo = $input['logo'];
            $extension = $logo->getClientOriginalExtension();
            $size = $logo->getClientSize();
            $file_name = $logo->getClientOriginalName();

            if($extension != "jpg" && $extension != "png" && $extension != "jpeg" && $extension != "gif")
            {
                return back()->with('error', 'Only jpgs, pngs, jpegs, and gifs are supported!');
            }

            //limits to 500kb for now, can be changed
            if($size > 500000)
            {
                return back()->with('error', 'Image is too large!');
            }

            $file_name = sha1($file_name.time()).'_'.$file_name;
            Storage::disk('team_logos')->put($file_name, File::get($logo));

            $team->logo_location = $file_name;
        }

        

        if($request->has('lfm'))
            $recruit_status = 1;
        else
            $recruit_status = 0;

        foreach($roles as $role)
        {
            if($request->has($role->role_id))
            {
                $role->selected = 1;
            }
            else
            {
                $role->selected = 0;
            }
            $role->save();
        }

        $team->recruiting = $recruit_status;
        $team->name = $name;
        if($team->save())
        {
            return back()->with('success', 'Successfully updated!');
        }
        return back()->with('error', 'Problem saving team details!');
    }

    public function messagePlayer($user_id)
    {
        $player = User::find($user_id);

        return view('messagePlayer')->withPlayer($player);
    }

    public function showTeam($team_id)
    {
        $team = Teams::find($team_id);

        return view('displayTeam')->withTeam($team);
    }

    public function leagues()
    {
        return view('comingSoon');
    }

    public function scrims()
    {
        return view('comingSoon');   
    }

    public function faq()
    {
        return view('comingSoon');
    }

    public function matches($type)
    {
        return view('comingSoon');
    }

    public function sendMessage(Request $request, $user_id)
    {
        $input = $request->all();

        $from_user = Auth::user();

        $to_user = User::find($user_id);

        $conversation_id = 0;

        $new_conversation = true;

        foreach($from_user->conversations as $conversation)
        {
            $users = $conversation->users;

            foreach($users as $user)
            {
                if($user->id == Auth::id())
                {   
                    continue;
                }
                else if($user->id == $user_id)
                {
                    $conversation_id = $conversation->id;
                }

            }
            if($conversation_id !== 0)
            {
                $new_conversation = false;
                break;
            }
        }

        if($new_conversation)
        {
            $conversation = new Conversations();
            $conversation->save();
            $conversation_id = $conversation->id;

            $conversation_user_pivot = new ConversationUserPivot();
            $conversation_user_pivot->user_id = $from_user->id;
            $conversation_user_pivot->conversations_id = $conversation_id;
            $conversation_user_pivot->save();

            $conversation_user_pivot = new ConversationUserPivot();
            $conversation_user_pivot->user_id = $user_id;
            $conversation_user_pivot->conversations_id = $conversation_id;
            $conversation_user_pivot->save();
        }



        $message = $input['message'];

        if($message == null)
            $message = "I forgot to put a message before hitting send kappa";

        $message_obj = new Messages();

        $message_obj->message = $message;
        $message_obj->from_id = $from_user->id;
        $message_obj->conversations_id = $conversation_id;

        if($message_obj->save())
        {
            $to_user_data = $to_user->userData;
            if($to_user_data->notification_token !== null)
            {
                $push = new Push();
                $firebase = new Firebase();

                $push->setTitle($from_user->name);
                $push->setMessage($message);
                $push->setConversationId($conversation_id);
                $push->setIsBackground(false);
                $push->setClickAction("Conversation_Details");
                $push->setIcon("demo_logo");
                //$push->setImage("demo_logo_trans_noti");

                $json = $push->getPush();

                $response = $firebase->send($to_user_data->notification_token, $json);
            }
            return back()->withCreated(true);
        }

        return back()->withCreated(false);
    }

    public function viewConversation($conversation_id)
    {
        $conversation = Conversations::find($conversation_id);

        $not_logged_in_user = 0;

        foreach($conversation->users as $user)
        {   
            if($user->id != Auth::id())
            {
                $not_logged_in_user = $user;
            }
        }

        return view('displayConversation')->withConversation($conversation)->withOther_user($not_logged_in_user);
    }

    public function joinTeam($team_id)
    {
        $user_id = Auth::id();

        $joinRequest = new TeamJoinRequests();

        $joinRequest->user_id = $user_id;
        $joinRequest->team_id = $team_id;

        if($joinRequest->save())
            return back()->withSent(true);            

        return back()->withSent(false);
    }

    public function invitePlayer($user_id, $team_id)
    {
        $invite = new PlayerInvites();
        $invite->user_id = $user_id;
        $invite->team_id = $team_id;

        if($invite->save())
        {
            $teamRequest = TeamJoinRequests::where('user_id', $user_id)->where('team_id', $team_id)->delete();
            return back()->withSent(true);            
        }

        return back()->withSent(false);
    }

    public function manageInviteResponse($user_id, $team_id, $choice)
    {
        if($choice == "accept")
        {
            $pivot = new TeamUserPivot();
            $pivot->teams_id = $team_id;
            $pivot->user_id = $user_id;    

            if($pivot->save())
            {
                $invite = PlayerInvites::where('user_id', $user_id)->where('team_id', $team_id)->delete();
                return back()->withManaged(true);            
            }
        }   
        else
        {
            $invite = PlayerInvites::where('user_id', $user_id)->where('team_id', $team_id)->delete();
            return back()->withManaged(true);
        }
        return back()->withManaged(false);
    }    
}
