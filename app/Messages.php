<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = [
        'from_id', 'conversations_id', 'message'
    ];

    public function from()
    {
    	return $this->belongsTo(User::class, 'from_id');
    }

    public function conversation()
    {
    	return $this->belongsTo(Conversations::class, 'conversations_id');
    }

    
    
}
