<?php
 
namespace App;

/**
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Push {
 
    // push message title
    private $title;
    private $message;
    private $image;
    private $click_action;
    private $convo_id;
    private $icon;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;
 
    function __construct() {
         
    }
 
    public function setTitle($title) {
        $this->title = $title;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }

    public function setIcon($icon) {
        $this->icon = $icon;
    }
 
    public function setPayload($data) {
        $this->data = $data;
    }
 
    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }

    public function setClickAction($click_action) {
        $this->click_action = $click_action;
    }

    public function setConversationId($convo_id) {
        $this->convo_id = $convo_id;
    }
 
    public function getPush() {
        $res = array();
        $res['data']['title'] = $this->title;
        $res['data']['is_background'] = $this->is_background;
        $res['data']['message'] = $this->message;
        $res['data']['image'] = $this->image;
        $res['data']['payload'] = $this->data;
        $res['data']['convo_id'] = $this->convo_id;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        $res['notification']['body'] = $this->message;
        $res['notification']['title'] = $this->title;
        $res['notification']['icon'] = $this->icon;
        $res['notification']['click_action'] = $this->click_action;
        return $res;
    }
 
}