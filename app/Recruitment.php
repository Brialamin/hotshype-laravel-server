<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recruitment extends Model
{

    protected $fillable = [
        'user_id', 'team_id', 'role_id', 'checked'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function team()
    {
        return $this->belongsTo(Teams::class);
    }
    
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
