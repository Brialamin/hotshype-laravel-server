<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function recruitment()
    {
        return $this->hasMany(Recruitment::class);
    }
}
