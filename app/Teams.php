<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'logo_location', 'recruiting', 'captain'
    ];

    public function players()
    {
    	return $this->belongsToMany(User::class, 'team_user_pivots');
    }

    public function teamCaptain()
    {
    	return $this->belongsTo(User::class, 'captain');
    }

    public function invites()
    {
        return $this->hasMany(PlayerInvites::class, 'team_id');
    }

    public function joinRequests()
    {
        return $this->hasMany(TeamJoinRequests::class, 'team_id');
    }

    public function lookingForRoles()
    {
        return $this->hasMany(Recruitment::class, 'team_id');
    }
}
