<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function teams()
    {
        return $this->belongsToMany(Teams::class, 'team_user_pivots');
    }

    public function userData()
    {
        return $this->hasOne(UserData::class);
    }

    public function conversations()
    {
        return $this->belongsToMany(Conversations::class, 'conversation_user_pivots');
    }

    public function invites()
    {
        return $this->hasMany(PlayerInvites::class);
    }

    public function teamRequests()
    {
        return $this->hasMany(TeamJoinRequests::class);
    }

    public function roles()
    {
        return $this->hasMany(Recruitment::class);
    }
    
}
