<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{

	protected $table = 'user_data';

    protected $fillable = [
        'user_id', 'last_active', 'twitch_name', 'bnet_tag', 'posts', 'is_streamer', 'recruitable', 'notification_token'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public static function updateLastActive($user_id)
    {
    	$user = UserData::where('user_id', $user_id)->first();
    	$user->last_active = new DateTime('now');


    	if($user->save())
    	{
    		return true;
    	}

    	return false;
    }

    public static function updatePictureCount($user_id)
    {
    	$user = UserData::where('user_id', $user_id)->first();

    	if($user->increment('posts'))
    	{
    		return true;
    	}

    	return false;
    }

    public static function updateTwitchAccount($user_id, $twitch_name)
    {
    	$user = UserData::where('user_id', $user_id)->first();
    	$user->twitch_name = $twitch_name;

    	if($user->save())
    	{
    		return true;
    	}

    	return false;
    }

    public static function updateBNetAccount($user_id, $bnet_name)
    {
    	$user = UserData::where('user_id', $user_id)->first();
    	$user->bnet_tag = $bnet_name;

    	if($user->save())
    	{
    		return true;
    	}

    	return false;
    }

    public static function updateIsStreamer($user_id, $streamer_flag)
    {
    	$user = UserData::where('user_id', $user_id)->first();
    	$user->is_streamer = $streamer_flag;

    	if($user->save())
    	{
    		return true;
    	}

    	return false;
    }

    public static function updateIsRecruitable($user_id, $recruit_flag)
    {
    	$user = UserData::where('user_id', $user_id)->first();
    	$user->recruitable = $recruit_flag;

    	if($user->save())
    	{
    		return true;
    	}

    	return false;
    }
}
