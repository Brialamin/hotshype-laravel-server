<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->string('name');
            $table->string('logo_location')->nullable();
            $table->integer('captain')->unsigned();
            $table->integer('player_1')->unsigned()->nullable();
            $table->integer('player_2')->unsigned()->nullable();
            $table->integer('player_3')->unsigned()->nullable();
            $table->integer('player_4')->unsigned()->nullable();
            $table->integer('player_5')->unsigned()->nullable();
            $table->integer('player_6')->unsigned()->nullable();
            $table->integer('player_7')->unsigned()->nullable();  
        });
        Schema::table('teams', function ($table) {
            $table->foreign('captain')->references('id')->on('users');
            $table->foreign('player_1')->references('id')->on('users');
            $table->foreign('player_2')->references('id')->on('users');
            $table->foreign('player_3')->references('id')->on('users');
            $table->foreign('player_4')->references('id')->on('users');
            $table->foreign('player_5')->references('id')->on('users');
            $table->foreign('player_6')->references('id')->on('users');
            $table->foreign('player_7')->references('id')->on('users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
