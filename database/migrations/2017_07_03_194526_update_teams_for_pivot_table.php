<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTeamsForPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropForeign(['player_1']);
            $table->dropForeign(['player_2']);
            $table->dropForeign(['player_3']);
            $table->dropForeign(['player_4']);
            $table->dropForeign(['player_5']);
            $table->dropForeign(['player_6']);
            $table->dropForeign(['player_7']);
            $table->dropColumn(['player_1','player_2','player_3','player_4','player_5','player_6','player_7']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teams', function ($table) {
            $table->integer('player_1')->unsigned()->index();
            $table->integer('player_2')->unsigned()->index();
            $table->integer('player_3')->unsigned()->index();
            $table->integer('player_4')->unsigned()->index();
            $table->integer('player_5')->unsigned()->index();
            $table->integer('player_6')->unsigned()->index();
            $table->integer('player_7')->unsigned()->index();

            $table->foreign('player_1')->references('id')->on('users');
            $table->foreign('player_2')->references('id')->on('users');
            $table->foreign('player_3')->references('id')->on('users');
            $table->foreign('player_4')->references('id')->on('users');
            $table->foreign('player_5')->references('id')->on('users');
            $table->foreign('player_6')->references('id')->on('users');
            $table->foreign('player_7')->references('id')->on('users'); 
        });
    }
}
