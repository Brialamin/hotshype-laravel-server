<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveTeamsCaptainToPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropForeign(['captain']);
            $table->dropColumn('captain');
        });
        Schema::table('team_user_pivots', function (Blueprint $table) {
            $table->boolean('is_captain')->default(0);
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('teams', function ($table) {
            $table->integer('captain')->unsigned()->index();
        });
        
        Schema::table('teams', function ($table) {
            $table->foreign('captain')->references('id')->on('users'); 
        });

        Schema::table('team_user_pivots', function (Blueprint $table) {
            $table->dropColumn('is_captain');
        }); 
    }
}
