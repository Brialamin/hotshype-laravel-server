<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePivotTablesForLaravelColumnNamingConvention extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_user_pivots', function (Blueprint $table) {
            $table->renameColumn('team_id', 'teams_id');
        }); 

        Schema::table('conversation_user_pivots', function (Blueprint $table) {
            $table->renameColumn('conversation_id', 'conversations_id');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_user_pivots', function (Blueprint $table) {
            $table->renameColumn('teams_id', 'team_id');
        }); 

        Schema::table('conversation_user_pivots', function (Blueprint $table) {
            $table->renameColumn('conversations_id', 'conversation_id');
        }); 
    }
}
