@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
    	<?php $previousUser = null;
    		  $currentMessageBlock;
    		  $pullDirection; ?>
        @foreach($conversation->messages as $message)
        	{{-- The current message is from the same user as the last message --}}
			@if($message->from->id == $previousUser)
				@if($previousUser == Auth::id())
					<?php $pullDirection = "message-right"; ?>
				@else
					<?php $pullDirection = "message-left"; ?>
				@endif
				<script>
                    $('#{{"message_start_".$currentMessageBlock}}').find(".panel-body").find("{{'.'.$pullDirection}}").append("<br>{{$message->message}}&nbsp;");
                </script>
    		@else
    			<?php $currentMessageBlock = $message->id; $previousUser = $message->from->id; ?>
    			<div class="row">
    				<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default dark_border" id={{ "message_start_".$message->id }}>
				        	@if($message->from->id != Auth::id())
					        	<div class="panel-heading clearfix dark_border dark_panel_head inv-text">
				        			<div class="pull-left">
			        					{{ $message->from->name." @ ".date('m/d/Y h:i:a', $message->created_at->timestamp) }}
			    					</div>
			        			</div>
			        			<div class="panel-body dark_border dark_panel_body_message_left">
			        				<div class="message-left">
					        			{{ $message->message }}
					        		</div>
					        	</div>
						    @else
						    	<div class="panel-heading dark_panel_head dark_border inv-text clearfix">
				        			<div class="pull-right">
			        					{{ $message->from->name." @ ".date('m/d/Y h:i:a', $message->created_at->timestamp) }}
			    					</div>
			        			</div>
			        			<div class="panel-body dark_panel_body dark_border inv-text">
			        				<div class="message-right">
					        			{{ $message->message }}
					        		</div>
					        	</div>
						    @endif
			    		</div>
    				</div>
    			</div>
				
    		@endif	
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
	        {!! Form::open(['action' => ['HomeController@sendMessage', $other_user->id]]) !!}
	        	{{ Form::label('message', "Reply", ['class'=>'inv-text']) }}
	            {{ Form::textarea('message', null, ['rows' => 5, 'cols' => 76]) }}
	            <br>
	            <br>
	        <div class="pull-right">
		            {{ Form::submit('Reply') }}
		        {!! Form::close() !!}
	        </div>
    	</div>
	</div>
@endsection