@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
            <?php $playerIsOnTeam = false; ?>
            <?php $requestedTeamsToJoin = array(); ?>
            @if(Auth::user())
                @foreach(Auth::user()->teamRequests as $teamRequest)
                    <?php array_push($requestedTeamsToJoin, $teamRequest->team_id); ?>
                @endforeach
                @foreach(Auth::user()->invites as $invite)
                    <?php array_push($requestedTeamsToJoin, $invite->team_id); ?>
                @endforeach
            @endif
            <div class="row col-md-10 col-md-offset-1">
                <div class="panel panel-default dark_border">
                    <div class="panel-heading clearfix dark_panel_head dark_border">
                        <div class="pull-left inv-text">
                            {{ $team->name }}
                        </div>
                        <div class="pull-right inv-text">
                            Captain: {{ $team->teamCaptain->name }}
                        </div>
                    </div>

                    <div class="panel-body dark_panel_body dark_border inv-text">
                        <div class="row underline">
                            <div class="col-md-4">
                                <img src="{{ asset("storage/team_logos/".$team->logo_location) }}"></img>
                            </div>
                            <div class="col-md-4">
                                <p>Team Members</p>
                                <ul>
                                @foreach($team->players as $player)
                                    @if(Auth::id() == $player->id)
                                        <?php $playerIsOnTeam = true; ?>
                                    @endif
                                    <li><a href="{{ url("users/".$player->id) }}" class="pull-left">{{ $player->name }}</a></li>
                                @endforeach
                                </ul>
                                @if($team->recruiting)
                                    <p>Looking for roles</p>
                                    <ul>
                                    @foreach($team->lookingForRoles as $roles)
                                        @if($roles->selected)
                                            <li>{{$roles->role->name}}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <p>Team Details</p>
                                <span>Established: {{ date('m/d/Y', strtotime($team->created_at)) }}</span>
                                <br>
                                <span>Division: N/A</span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-1 col-md-offset-11">
                                @if(in_array($team->id, $requestedTeamsToJoin))
                                    <button class="btn btn-primary btn-sm pull-right" disabled>Invite Pending</button>
                                @elseif(!$playerIsOnTeam && Auth::user())
                                    <a href="{{ url("teams/".$team->id."/join") }}" class="btn btn-default btn-sm pull-right">Request to Join</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

