@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
        <div class="col-md-6 img-center">
            <img src="images/home_image_1.png" height="250" width="450" class=>
        </div>
        <div class="col-md-6 center-block inv-text">
            <h1 align="center">Teamwork makes the Dreamwork</h1>
            <br/>
            <p align="center">Join your friends and create a team to compete!  If your friends abandoned you, no worries!  You can search for teams to join and apply to them.</p>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-6 center-block inv-text">
            <h1 align="center">Register for a division</h1>
            <br/>
            <p align="center">Once you've found your team, you can get ready to compete! Join one of the three divisions to compete against other teams.  The divisions are from D1 to D3, with D1 being the top and D3 being the bottom.</p>
        </div>
        <div class="col-md-6 img-center">
            <img src="images/divisions.png" height="250" width="450">
        </div>
    </div>
    <hr />
    <div class="row" style="margin-top:75px">
        <div class="col-md-6 img-center">
            <img src="images/practice.png" height="250" width="450" class=>
        </div>
        <div class="col-md-6 center-block inv-text">
            <h1 align="center">Practice, Practice, Practice</h1>
            <br/>
            <p align="center">Now that you're registered, we'll take care of the rest for you.  Your team should worry about practicing to be the very best, like no one ever was.  We'll handle finding a closely ranked opponent for each week.</p>
        </div>
    </div>
@endsection