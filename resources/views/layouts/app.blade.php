<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-inverse navbar-fixed-top no-underline" data-spy="affix" data-offset-top="197">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a href="{{ url('/') }}">
                        <img src="/images/demo_logo.png" height="50" width="50"> 
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Play
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu inverse-dropdown">
                                <li><a href="{{ url('/league') }}">League Play</a></li>
                                <li><a href="{{ url('/scrims') }}">Friendlies / Scrims</a></li>
                                <li><a href="{{ url('/faq') }}">Rules</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Recruiting
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu inverse-dropdown">
                                <li><a href="{{ url('/recruit/player') }}">Looking for Team</a></li>
                                <li><a href="{{ url('/recruit/team') }}">Looking for Players</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Matches
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu inverse-dropdown">
                                <li><a href="{{ url('/matches/upcoming') }}">Upcoming</a></li>
                                <li><a href="{{ url('/matches/live') }}">Live</a></li>
                                <li><a href="{{ url('/matches/recent') }}">Recent</a></li>
                            </ul>
                        </li>
                        @if (!Auth::guest())
                            <li><a href="{{ url('/teams/new') }}">Create a Team</a></li>
                        @endif    
                        <li><a href="{{ url('/forum') }}">Forums</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu inverse-dropdown" role="menu">
                                    <li>
                                        <a href="{{ url('/users/'.Auth::id().'/teams') }}">
                                            Your Teams
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/users/'.Auth::id()) }}">
                                            Your Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            @yield('content')
        </div>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
