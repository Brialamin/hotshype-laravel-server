@extends('layouts.app')

@section('content')
    {{-- If user is not team captain, they shouldn't be able to manage the team --}}
    @if($team->teamCaptain->id !== Auth::id())
        <script>document.location.href = "/";</script>
    @endif
    <div class="container" style="margin-top:75px">
        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @elseif(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <h3 class="inv-text">{{ $team->name }}</h3>
        {!! Form::open(['action' => ['HomeController@updateTeam', $team->id], 'files' => true]) !!}
            <div class="col-md-6 clearfix">
                <div class="panel panel-default dark_border inv-text">
                    <div class="panel-heading dark_panel_head dark_border inv-text">General</div>

                    <div class="panel-body dark_border dark_panel_body">
                        <div class="row">
                            <div class="col-xs-3" style="margin-top:5px">
                                {{ Form::label('new_name', 'Name: ') }}
                            </div>
                            <div class="col-xs-6">
                                {{ Form::text('new_name', $team->name, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-3" style="margin-top:-2.5px">
                                {{ Form::label('logo', 'Team Logo (optional)') }}
                            </div>
                            <div class="col-xs-6" style="margin-top:-5px">
                                {{ Form::file('logo') }}
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; margin-top: 10px">
                            @if($team->recruiting)
                                {{ Form::checkbox('lfm', 'lfm', true) }}
                            @else
                                {{ Form::checkbox('lfm', 'lfm') }}
                            @endif
                            {{ Form::label('lfm', 'Recruiting Players? ') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 clearfix">
                <div class="panel panel-default dark_border inv-text">
                    <div class="panel-heading dark_border dark_panel_head inv-text">Roles needed</div>

                    <div class="panel-body dark_border dark_panel_body inv-text">
                        @foreach ($roles as $role)
                            @if($role->selected)
                                {{ Form::checkbox($role->role->id, $role->role->id, true) }}
                            @else
                                {{ Form::checkbox($role->role->id, $role->role->id) }}
                            @endif
                            {{ Form::label($role->role->id, $role->role->name) }}
                            <br>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-6 clearfix">
                <div class="panel panel-default dark_border inv-text" style="margin-top: -110px">
                    <div class="panel-heading dark_border dark_panel_head inv-text">Players</div>

                    <div class="panel-body dark_border dark_panel_body inv-text">
                        <table class="table">
                            @foreach ($team->players as $player)
                                <tr>
                                    <td class="underline">
                                        <a href="{{ url("users/".$player->id) }}" class="pull-left">{{ $player->name }}</a>
                                    </td>
                                    <td align="right">
                                        @if($player->id !== Auth::id() && $team->teamCaptain->id === Auth::id())
                                            <a href="{{ url("message/user/".$player->id) }}" class="btn btn-primary btn-sm">Message</a>
                                            <a href="{{ url("message/user/".$player->id) }}" class="btn btn-danger btn-sm">Remove</a>   
                                        @else
                                            <a href="{{ url("message/user/".$player->id) }}" class="btn btn-danger btn-sm" style="color: #ffffff;">Leave Team</a>   
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
               </div>
            </div>
            <div class="col-md-6 clearfix">
                <div class="panel panel-default dark_border inv-text">
                    <div class="panel-heading dark_border dark_panel_head inv-text">Prospective Players</div>

                    <div class="panel-body dark_border dark_panel_body inv-text">
                        @if(sizeof($requests) > 0 || sizeof($invites) > 0) 
                            <h4>Requests</h4>
                            <table class="table">
                                @foreach ($requests as $request)
                                    <tr>
                                        <td class="underline">
                                            <a href="{{ url("users/".$request->user->id) }}" class="pull-left">{{ $request->user->name }}</a>   
                                        </td>
                                        <td align="right">
                                            <a href="{{ url("message/user/".$request->user->id) }}" class="btn btn-primary btn-sm">Message</a>
                                            <a href="{{ url("user/".$request->user->id."/invite/team/".$team->id) }}" class="btn btn-success btn-sm">Invite</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <hr>
                            <h4>Invited</h4>
                            <table class="table">
                                @foreach ($invites as $invite)
                                    <td class="underline">
                                        <a href="{{ url("users/".$invite->user->id) }}" class="pull-left">{{ $invite->user->name }}</a>
                                    </td>
                                    <td align="right">
                                        <a href="{{ url("message/user/".$invite->user->id) }}" class="btn btn-primary btn-sm">Message</a>
                                    </td>
                                @endforeach
                            </table>
                        @else
                            <h4>No requests to join this team</h4>
                        @endif
                    </div>
                </div>
            </div>
        {{ Form::button('Save',array('class' => 'btn btn-success btn-md align-right', 'type' => 'submit', 'style' => 'margin-right: 15px')) }}
        {!! Form::close() !!}
    </div>
@endsection
