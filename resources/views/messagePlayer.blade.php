@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">New Message</div>

                <div class="panel-body">
                    {!! Form::open(['action' => ['HomeController@sendMessage', $player->id]]) !!}
                        {{ Form::label('message', $player->name ) }}
                        <br>
                        {{ Form::textarea('message') }}
                        <br>
                        <br>
                        {{ Form::submit('Send!') }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
