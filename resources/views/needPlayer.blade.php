@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
        <?php $previousTeam = 0; ?>
        @foreach ($roles as $teamRole)
            @if ($teamRole->team_id != $previousTeam)
                <?php $previousTeam = $teamRole->team_id; ?>
                </div>
                <div class="panel panel-default dark_border">
                    <div class="panel-heading clearfix dark_panel_head dark_border">
                        <span class="pull-left"><a href="{{ url('/teams/'.$teamRole->team_id) }}">{{ $teamRole->team->name }}</a></span>
                        <span class="pull-right inv-text">Captain: {{ $teamRole->team->teamCaptain->name }}</span>
                    </div>

                    <div class="panel-body dark_panel_body inv-text dark_border" id="{{ $teamRole->team_id }}">
                       {{$teamRole->role->name}}
                    </div>
            @elseif ($teamRole->team_id == $previousTeam)
                <script>
                    $('#{{$teamRole->team_id}}').append("- {{ $teamRole->role->name }} " )
                </script>
            @endif
        @endforeach
    </div>
@endsection