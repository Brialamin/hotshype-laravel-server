@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
        <?php $previousPlayer = 0; ?>
        @foreach ($roles as $playerRole)
            @if ($playerRole->user_id != $previousPlayer)
                <?php $previousPlayer = $playerRole->user_id; ?>
                </div>
                <div class="panel panel-default dark_border">
                    <div class="panel-heading dark_panel_head dark_border"><a href="{{ url('/users/'.$playerRole->user_id) }}">{{ $playerRole->user->name }}</a></div>

                    <div class="panel-body dark_panel_body inv-text dark_border" id="{{ $playerRole->user_id }}">
                       {{$playerRole->role->name}}
                    </div>
            @elseif ($playerRole->user_id == $previousPlayer)
                <script>
                    $('#{{$playerRole->user_id}}').append("- {{ $playerRole->role->name }} " )
                </script>
            @endif
        @endforeach
    </div>
@endsection