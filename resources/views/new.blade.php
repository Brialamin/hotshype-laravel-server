@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
        <div class="row">
            <div class="panel panel-default dark_border">
                <div class="panel-heading dark_panel_head inv-text dark_border">New Team</div>

                <div class="panel-body dark_panel_body inv-text dark_border">
                    {!! Form::open(['action' => 'HomeController@createTeam', 'files' => true]) !!}
                        {{ Form::label('new_name', 'Name: ') }}
                        {{ Form::input('text', 'new_name', null, ['class' => 'mform']) }}
                        <br>
                        <br>
                        {{ Form::submit('Create Team!', ['class' => 'btn btn-primary']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
