@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top:75px">
        Passport Clients
        <br>
        <passport-clients></passport-clients>
        <br>
        Authorized Clients
        <br>
        <passport-authorized-clients></passport-authorized-clients>
        <br>
        Personal Access Tokens
        <br>
        <passport-personal-access-tokens></passport-personal-access-tokens>
    </div>
@endsection