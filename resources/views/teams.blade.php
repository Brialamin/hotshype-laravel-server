@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
        @foreach ($teams as $team)
            <div class="panel panel-default inv-text dark_border">
                <div class="panel-heading clearfix dark_panel_head dark_border">
                    <h4 class="pull-left inv-text"><a href="{{ url('/teams/'.$team->id) }}">{{ $team->name }}</a></h4>
                    <div class="pull-right no-underline inv-text" style="padding-top: 5px;">
                        @if(Auth::id() == $team->teamCaptain->id)
                            <a href="{{ url("teams/".$team->id."/manage") }}" class="btn dark_button btn-default btn-sm">Manage</a>
                        @endif
                    </div>

                </div>

                <div class="panel-body dark_panel_body dark_border">
                   Captain: {{ $team->teamCaptain->name }}
                </div>
            </div>
        @endforeach
    </div>
@endsection