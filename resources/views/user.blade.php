@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:75px">
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @elseif(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        <h3 class="inv-text">{{ $name }}
        @if (Auth::id() != $id)
            <a href="{{ url("message/user/".$id) }}" class="btn btn-default dark_button btn-sm">Message</a>
        @endif
        </h3>
        <div class="col-md-3 inv-text no-underline">
            <ul class="nav nav-pills nav-stacked dark-back rounded-corner" id="userMenu">
                <li class="active"><a class="inv-text" data-toggle="pill" href="#general">General</a></li>
                <li><a class="inv-text" data-toggle="pill" href="#roles">Roles</a></li>
                @if (Auth::id() == $id)
                    <li><a class="inv-text" data-toggle="pill" href="#messages">Messages</a></li>
                    <li><a class="inv-text" data-toggle="pill" href="#invites">Team Invites</a></li>
                    <li><a class="inv-text" data-toggle="pill" href="#pending">Pending Team Requests</a></li>
                    <li><a class="inv-text" data-toggle="pill" href="#manage">Manage</a></li>
                @endif
            </ul>
        </div>

        <div class="tab-content col-md-9" style="margin-top:-25px">
            <div id="general" class="tab-pane inv-text fade in active">
                @include('user.general')
            </div>
            <div id="roles" class="tab-pane inv-text fade">
                @include('user.roles')
            </div>
            <div id="messages" class="tab-pane inv-text fade">
                @include('user.messages')
            </div>
            <div id="invites" class="tab-pane inv-text fade">
                @include('user.invites')
            </div>
            <div id="pending" class="tab-pane inv-text fade">
                @include('user.pending')
            </div>
            <div id="manage" class="tab-pane fade">
                @include('user.manage')
            </div>

          </div>
        
    </div>
@endsection