<h3>General Information</h3>
<p>
	Battle.net Tag: {{ $data->bnet_tag }}
	<br>
	Twitch account: <a href="http://www.twitch.tv/{{ $data->twitch_name }}">{{ $data->twitch_name }}</a>
</p>