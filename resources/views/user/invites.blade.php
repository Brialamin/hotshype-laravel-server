<h3 class="inv-text">Team Invites</h3>
<table class="table">
@foreach($invites as $invite)
	<tr>
		<td>
			<a href="{{ url("teams/".$invite->team_id) }}">{{ $invite->team->name }}</a>
		</td>
		<td align="right">
	        <a href="{{ url("message/user/".$invite->team->teamCaptain->id) }}" class="btn btn-default btn-sm">Message Captain</a>
	        <a href="{{ url("user/".$invite->user_id."/invite/team/".$invite->team_id."/accept") }}" class="btn btn-default btn-sm">Accept</a>
	        <a href="{{ url("user/".$invite->user_id."/invite/team/".$invite->team_id."/decline") }}" class="btn btn-default btn-sm">Decline</a>
	    </td>
    </tr>
@endforeach
</table>