{!! Form::open(['action' => ['HomeController@updateUser', Auth::id()]]) !!}
    <h3 class="inv-text">Profile Settings</h3>
    <div class="panel panel-default dark_border inv-text">
        <div class="panel-heading dark_border inv-text dark_panel_head">General</div>

        <div class="panel-body dark_border inv-text dark_panel_body">

            {{ Form::label('bnet', 'Battle.net Tag: ') }} &nbsp;&nbsp;
            {{ Form::input('text', 'bnet', $data->bnet_tag, ['class' => 'mform']) }}
            <br>
            <br>
            {{ Form::label('twitch', 'Twitch.tv Name: ') }}
            {{ Form::input('text', 'twitch', $data->twitch_name, ['class' => 'mform']) }}
            <br>
            @if($data->recruitable)
                {{ Form::checkbox('lft', 'lft', true) }}
            @else
                {{ Form::checkbox('lft', 'lft') }}
            @endif
            {{ Form::label('lft', 'Looking for Team? ') }}
            <br>
        </div>
    </div>
    <div class="panel panel-default dark_border inv-text">
        <div class="panel-heading dark_border inv-text dark_panel_head">Roles</div>

        <div class="panel-body dark_border inv-text dark_panel_body">
            @foreach ($roles as $role)
            @if($role->selected)
                {{ Form::checkbox($role->role->id, $role->role->id, true) }}
            @else
                {{ Form::checkbox($role->role->id, $role->role->id) }}
            @endif
            {{ Form::label($role->role->id, $role->role->name) }}
            <br>
            @endforeach
        </div>
    </div>
    {{ Form::button('Save',array('class' => 'submit-btn inv-text', 'type' => 'submit')) }}
{!! Form::close() !!}