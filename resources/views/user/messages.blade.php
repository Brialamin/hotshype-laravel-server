<h3>Messages</h3>
@foreach($conversations as $conversation)
	@foreach($conversation->users as $user)
		@if($user->id != Auth::id())
			<div class="panel panel-default dark_border inv-text">
		        <div class="panel-heading dark_border inv-text dark_panel_head"><a href="{{ url('/conversations/'.$conversation->id) }}">{{ $user->name }}</a></div>

		        <div class="panel-body dark_border inv-text dark_panel_body">
		        	{{ $conversation->lastMessage->from->name.": ".$conversation->lastMessage->message." @ ".date('m/d/Y h:i:a', $conversation->lastMessage->created_at->timestamp) }}
		        </div>
		    </div>
		@endif
		
	@endforeach
@endforeach