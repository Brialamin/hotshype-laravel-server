<h3 class="inv-text">Pending Team Requests</h3>
<table class="table">
@foreach($requests as $request)
	<tr>
		<td>
			<a href="{{ url("teams/".$request->team_id) }}">{{ $request->team->name }}</a>
		</td>
		<td align="right">
	        <a href="{{ url("message/user/".$request->team->teamCaptain->id) }}" class="btn btn-default btn-sm">Message Captain</a>
	    </td>
    </tr>
@endforeach
</table>