<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function() {
	Route::post('users/{user}/update', 'API\UserController@updateUser');
	Route::post('roles/{role}/update', 'API\RecruitmentController@updateRole');
	Route::get('users/{user}/messages', 'API\UserController@getUserMessages');
	Route::get('conversation/{conversation}', 'API\MessageController@loadMessages');
	Route::post('messages/create', 'API\MessageController@createMessage');
	Route::post('/users/{user_id}/token/update', 'API\UserController@updateUserNotificationToken');
});

Route::get('teams', 'API\TeamController@index');
Route::get('teams/recruiting', 'API\TeamController@lookingForPlayers');
Route::get('teams/{team}', 'API\TeamController@show');

Route::get('users', 'API\UserController@index');
Route::get('users/recruitable', 'API\UserController@getRecruitableUsers');
Route::get('users/{user}', 'API\UserController@show');
Route::get('users/{user}/teams', 'API\UserController@usersTeams');
Route::get('users/{user}/userData', 'API\UserController@getUserData');
Route::get('users/{user}/conversations', 'API\UserController@getUserConversations');

Route::get('conversation/{conversation}/users', 'API\MessageController@getUsers');
Route::get('conversation/{conversation}/lastmessage', 'API\MessageController@getLastMessage');

//Register user
Route::post('/users/register', 'API\UserController@register');