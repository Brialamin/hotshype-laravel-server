<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/teams/new', 'HomeController@new');

Route::post('/teams/create', 'HomeController@createTeam');

Route::get('/users/{user_id}', 'HomeController@userProfile');

Route::get('/users/{user_id}/teams', 'HomeController@usersTeams');

Route::post('/users/{user_id}/update', 'HomeController@updateUser');

Route::post('/users/{user_id}/roles/save', 'HomeController@saveRoles');

Route::get('/teams', 'HomeController@teams');

Route::get('/teams/{team_id}', 'HomeController@showTeam');

Route::get('/teams/{team_id}/manage', 'HomeController@manageTeam');

Route::post('/teams/{team_id}/update', 'HomeController@updateTeam');

Route::get('/teams/{team_id}/join', 'HomeController@joinTeam');

Route::get('user/{user_id}/invite/team/{team_id}', 'HomeController@invitePlayer');

Route::get('user/{user_id}/invite/team/{team_id}/{choice}', 'HomeController@manageInviteResponse');

Route::get('/recruit/player', 'HomeController@lookingForTeam');

Route::get('/recruit/team', 'HomeController@lookingForPlayers');

Route::get('/message/user/{user_id}', 'HomeController@messagePlayer');

Route::post('/message/user/{user_id}/send', 'HomeController@sendMessage');

Route::get('/conversations/{conversation_id}', 'HomeController@viewConversation');

Route::get('/league', 'HomeController@leagues');

Route::get('/scrims', 'HomeController@scrims');

Route::get('/faq', 'HomeController@faq');

Route::get('/matches/{type}', 'HomeController@matches');

Route::get('/test/passport', 'TestController@passport');

